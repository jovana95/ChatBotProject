﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Service
{
    public class DBService
    {
        ChatbotDBEntities4 db = new ChatbotDBEntities4();

        public List<Category> getCategory()
        {
            return db.Categories.ToList();
        }

        public List<ClassOfCategory> getClasses(int idCategory)
        {
            return db.ClassOfCategories.Where(x => x.id_category==idCategory).ToList();
        }

        public List<Manufacturer> getManufacturers(int idClass)
        {
            List<Manufacturer> manufacturers = (from v in db.Vehicles
                           join m1 in db.Model_vehicle on v.id_model equals m1.id
                           join m2 in db.Manufacturers on m1.id_manufacturer equals m2.id
                           where v.id_class == idClass
                           select m2).ToList();
            return manufacturers;          
        }

        public List<Model_vehicle> getModels(int idManufacturer)
        {
            List<Model_vehicle> model_vehicle = (from m in db.Model_vehicle
                                                 where m.id_manufacturer == idManufacturer
                                                 select m).ToList();
            return model_vehicle;
        }

        public List<Characteristic> getCharacteristic(int idModel)
        {
            List<Characteristic> characteristic = (from m in db.Model_vehicle
                                                 join c in db.Characteristics on m.id equals c.id_model
                                                 where m.id == idModel
                                                 select c).ToList();
            return characteristic;
        }

        public List<Package> getPackage(int idCharacteristic)
        {
            List<Package> package = (from c in db.Characteristics
                                     join p in db.Packages on c.id_package equals p.id
                                     where c.id==idCharacteristic
                                     select p).ToList();
            return package;
        }

    }
}