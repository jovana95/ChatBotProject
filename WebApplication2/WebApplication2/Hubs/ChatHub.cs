using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using WebApplication2.Models;

namespace WebApplication2.Hubs
{
    public class ChatHub : Hub
    {
        public static ConcurrentDictionary<string, MyUserType> MyUsers = new ConcurrentDictionary<string, MyUserType>();
        private object data;
        private static int ind = 0;
        private  Tree tree = new Tree();       
        static int i = 0;
        static int flag = 1;
        public Node pomocni;
        public  Node n = new Node(1, "Welcome to your chat bot asistant. Do you know the exact vehicle you want to buy ? ", 1, null, "");
        ChatbotDBEntities4 db = new ChatbotDBEntities4();
        private List<QuestionAnswer> questionAnswer = new List<QuestionAnswer>();
        public static bool edited = false;
        public Node prethodni = null;
        public static AnswerModel am = new AnswerModel();
        private static PurchaseRequest pr = new PurchaseRequest();

        public static List<Node> listQuestion = new List<Node>();
        public int globalId = 0;

        public override Task OnConnected()
        {
            if (flag == 1)
            {
                Node n1 = new Node(2, "Which type of vehicle category would you like?", 1, n, "");
                n.Left = n1;
                ////Kraj No 
                Node n2 = new Node(3, "Which type of vehicle class would you like?", 1, n1, "Category");
                n1.Left = n2;
                Node n3 = new Node(4, "Which type of vehicle manufacturer would you like?", 1, n2, "ClassOfCategory");
                n2.Left = n3;
                Node n4 = new Node(5, "Which type of vehicle model would you like?", 1, n3, "Manufacturer");
                n3.Left = n4;
                Node n5 = new Node(6, "Which type of vehicle characteristic would you like?", 1, n4, "Model_vehicle");
                n4.Left = n5;
                Node n6 = new Node(7, "Which type of vehicle package would you like?", 1, n5, "Characteristics");
                n5.Left = n6;
                Node n7 = new Node(8, "Do you want to buy the vehicle?", 1, n6, "Package");
                n6.Left = n7;
                //NO GRANA!!!!!!!
                Node n8 = new Node(9, "Which type of vehicle category would you like?", 1, n, "");
                n.Right = n8;
                Node n9 = new Node(10, "How much should the seat have?", 1, n8, "Category");
                n8.Left = n9;
                Node n10 = new Node(11, "How many kilometers do you average annually?", 1, n9, "CharacteristicSeats");
                n9.Left = n10;
                Node n11 = new Node(12, "Color?", 1, n10, "CharacteristicMileage");
                n10.Left = n11;
                Node n12 = new Node(13, "Benzin ili dizel?", 1, n11, "CharacteristicColor");
                n11.Left = n12;
                Node n13 = new Node(14, "Do you want an electric motor?", 1, n12, "PackageFuel");
                n12.Left = n13;
                Node n14 = new Node(15, "The year of production?", 1, n13, "PackageEngine");
                n13.Left = n14;
                Node n15 = new Node(16, "Cena?", 1, n14, "Model_vehicleYop");
                n14.Left = n15;
                Node n16 = new Node(17, "Enter your name", 1, n15, "PackagePrice");
                n15.Left = n16;
                Node n17 = new Node(18, "Enter your last name", 1, n16, "Name");
                n16.Left = n17;
                Node n18 = new Node(19, "Enter your address", 1, n17, "LastName");
                n17.Left = n18;
                Node n19 = new Node(20, "Enter your email", 1, n18, "Adress");
                n18.Left = n19;
                Node n20 = new Node(21, "Enter your phone number", 1, n19, "Email");
                n19.Left = n20;
                Node n21 = new Node(22, "Your request has been successfully created.", 1, n20, "PhoneNumber");
                n20.Left = n21;
                //Stablo();
                listQuestion.Add(n);
                listQuestion.Add(n1);
                listQuestion.Add(n2);
                listQuestion.Add(n3);
                listQuestion.Add(n4);
                listQuestion.Add(n5);
                listQuestion.Add(n6);
                listQuestion.Add(n7);
                listQuestion.Add(n8);
                listQuestion.Add(n9);
                listQuestion.Add(n10);
                listQuestion.Add(n11);
                listQuestion.Add(n12);
                listQuestion.Add(n13);
                listQuestion.Add(n14);
                listQuestion.Add(n15);
                listQuestion.Add(n16);
                listQuestion.Add(n17);
                listQuestion.Add(n18);
                listQuestion.Add(n19);
                listQuestion.Add(n20);
                listQuestion.Add(n21);
            }
            MyUsers.TryAdd(Context.ConnectionId, new MyUserType() { ConnectionId = Context.ConnectionId });
            return base.OnConnected();
        }


        public string CheckUser()
        {
            User user = db.Users.FirstOrDefault(x => x.Username == System.Web.HttpContext.Current.User.Identity.Name);
            if (user != null)
            {
                IQueryable questionAnswer = (from a in db.Answers
                                             join q in db.Questions on a.id_question equals q.id
                                             join t in db.Trees on q.id_tree equals t.id
                                             where t.id_user == user.id_user
                                             select new
                                             {
                                                 question = q.Name,
                                                 answer = a.Name,
                                                 id = q.id_ques_node,
                                                 ida = a.id_answer
                                             }).ToList().AsQueryable();

                ///Type t = typeof(Newtonsoft.Json.JsonConvert.SerializeObject(questionAnswer));
                System.Diagnostics.Debug.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(questionAnswer));
                return Newtonsoft.Json.JsonConvert.SerializeObject(questionAnswer);
            }
            return "";
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            MyUserType garbage;
            int idq = globalId;
            flag = 0;
            MyUsers.TryRemove(Context.ConnectionId, out garbage);
            return base.OnDisconnected(stopCalled);
        }


        public void Send(int id, string answ, int idInTable, string connId) //id pitanja, id izabranog polja, id konekcije
        {
            QuestionAnswer qa = null;
            int idq = id;
            System.Diagnostics.Debug.Write(answ);
            var list = listQuestion;
            if (id > 0)
                prethodni = list[id - 1];
            else
                prethodni = list[id];
            Node n = list[id];

            switch (n.nameOfTable)
            {
                case "Category":
                    am.SetIdCategory(idInTable);
                    pr.id_category = idInTable;
                    break;
                case "ClassOfCategory":
                    am.SetIdClass(idInTable);
                    pr.id_class = idInTable;
                    break;
                case "Manufacturer":
                    am.SetIdManufacturer(idInTable);
                    pr.id_manufacturer = idInTable;
                    break;
                case "Model_vehicle":
                    am.SetIdModel(idInTable);
                    pr.id_model = idInTable;
                    break;
                case "Characteristics":
                    am.SetIdCharacteristics(idInTable);
                    pr.id_characteristic = idInTable;
                    break;
                case "Package":
                    am.SetIdPackage(idInTable);
                    pr.id_package = idInTable;
                    break;
                case "CharacteristicSeats":
                    am.SetNumOfSeats(Int32.Parse(answ));
                    break;
                case "CharacteristicMileage":
                    am.SetMileage(double.Parse(answ, CultureInfo.InvariantCulture.NumberFormat));
                    break;
                case "CharacteristicColor":
                    am.SetColor(answ);
                    break;
                case "PackageFuel":
                    am.SetFuel(answ);
                    break;
                case "PackageEngine":
                    am.SetEngine(answ);
                    break;
                case "Model_vehicleYop":
                    am.SetYearOfProduction(Int32.Parse(answ));
                    break;
                case "PackagePrice":
                    if (listQuestion[0].getSelectedAnswer() == 0)
                    {
                        break;
                    }
                    else
                    {
                        am.SetPrice(double.Parse(answ, CultureInfo.InvariantCulture.NumberFormat));
                        var sql_upit = from cha in db.Characteristics
                                       join modelv in db.Model_vehicle
                                       on cha.id_model equals modelv.id
                                       join packg in db.Packages
                                       on cha.id_package equals packg.id
                                       where packg.engine == am.engine && cha.color == am.color && cha.mileage == am.mileage && cha.number_of_seats == am.num_of_seats && packg.fuel == am.fuel && cha.id_category == am.id_category && modelv.year_of_production == am.year_of_production && packg.Price == am.price

                                       select new
                                       {
                                           idpacka = packg.id,
                                           idchar = cha.id,
                                           idmode = modelv.id,
                                           idcate = cha.id_category
                                       };                       
                        
                        foreach (var sql in sql_upit)
                        {
                            pr.id_package = sql.idpacka;
                            pr.id_characteristic = sql.idchar;
                            pr.id_category = sql.idcate;
                            pr.id_model = sql.idmode;
                        }
                    }
                    break;
                case "Name":
                    pr.FirstName = answ;
                    break;
                case "LastName":
                    pr.LastName = answ;
                    break;
                case "Adress":
                    pr.Address = answ;
                    break;
                case "Email":
                    pr.Email = answ;
                    break;
                case "PhoneNumber":
                    pr.PhoneNumber = answ;
                    break;
                case "":
                    break;
                default:
                    break;

            }

            string answer = "";
            if (!answ.Equals(""))
            {

                answer = "Your answer is " + answ + "&nbsp;&nbsp;&nbsp;<button id='editing' OnClick='EditAnswer(" + id + "," + idInTable + ")' value=' '><span class='glyphicon glyphicon-pencil'></span></button>";
            }
            if (id < 16)
            {

                string que = "";
                List<Object> ans1 = new List<Object>();
                //ovaj if je kada se klikne na dugme za editovanje pitanja
          /*      if (answ.Equals("-"))
                {
                    edited = true;
                    User user1 = db.Users.FirstOrDefault(x => x.Username == System.Web.HttpContext.Current.User.Identity.Name);                    

                    if ((Node)HttpRuntime.Cache["node"] != null && ind == 0)
                    {
                        n = (Node)HttpRuntime.Cache["node"];
                        ind = 1;
                        flag = 1;
                    }

                    if (user1 != null)
                    {
                        //upit uzima stablo logovanog usera, kupi id pitanja 
                        var sql_upit = from t in db.Trees
                                       join u in db.Users
                                        on t.id_user equals u.id_user
                                       join qu in db.Questions
                                         on t.id equals qu.id_tree
                                       join an in db.Answers
                                       on qu.id equals an.id_question
                                       where t.id_user == user1.id_user && qu.id_ques_node == id

                                       select new
                                       {
                                           idq = qu.id_ques_node,
                                           ida = an.id_answer,
                                           question = qu.Name,
                                           answer = an.Name,
                                           question_valid = qu.isValid,
                                           answer_valid = an.isValid
                                       };
                        foreach (var sql in sql_upit)
                        {
                            n = list[sql.idq.Value - 1];
                            n.setSelectedAnswer(sql.ida.Value);
                            que = n.getQuestion();
                            ans1 = n.getAnswers(am);
                        }
                    }
       /*             else
                    {
                        var sql_upit = from t in db.Trees
                                       join qu in db.Questions
                                       on t.id equals qu.id_tree
                                       join an in db.Answers
                                       on qu.id equals an.id_question
                                       where qu.id_ques_node == id

                                       select new
                                       {
                                           idq = qu.id_ques_node,
                                           ida = an.id_answer,
                                           question = qu.Name,
                                           answer = an.Name,
                                           question_valid = qu.isValid,
                                           answer_valid = an.isValid
                                       };
              /*          foreach (var sql in sql_upit)
                        {
                            n = list[sql.idq.Value - 1];
                            n.setSelectedAnswer(sql.ida.Value);
                            que = n.getQuestion();
                            ans1 = n.getAnswers(am);
                        }*/
                    //}
                    
                    //n = n.parent;  


                    //n = list[id - 1];
                    //n.setSelectedAnswer(idInTable);
                    //string q = n.getQuestion();

        /*            HttpRuntime.Cache.Insert("questionAnswer", questionAnswer, null, DateTime.Now.AddMinutes(2200), Cache.NoSlidingExpiration);

                    Clients.Client(connId).addNewMessageToPage(answer, que, ans1);
                    return;

                }

                if (edited == true)
                {
                    id = id - 1;

                }*/

                if ((Node)HttpRuntime.Cache["node"] != null && ind == 0)
                {
                    n = (Node)HttpRuntime.Cache["node"];
                    ind = 1;
                    flag = 1;
                }
                n.setSelectedAnswer(idInTable);

                string question = n.getQuestion();
                List<Object> ans = n.getAnswers(am);
                User user = db.Users.FirstOrDefault(x => x.Username == System.Web.HttpContext.Current.User.Identity.Name);
                if (!answ.Equals(""))
                {

                    if (user != null)
                        qa = new QuestionAnswer(user.id_user, prethodni.getQuestion(), answ, prethodni.getIdQuestion(), idInTable, user.id_user);
                    else
                        qa = new QuestionAnswer(1009, prethodni.getQuestion(), answ, prethodni.getIdQuestion(), idInTable, 1009);
                }

                if (!answ.Equals(""))
                {
                    
                    //cuvanje u bazu
                    if (user == null)
                    {
                        //Tree tree = new Tree();
                        tree.id_user = qa.idUser;
                        db.Trees.Add(tree);
                        db.SaveChanges();

                        Question q = new Question();
                        q.isValid = true;
                        q.Name = qa.nameQuestion;
                        q.id_tree = tree.id;
                        q.id_ques_node = qa.id_question;
                        db.Questions.Add(q);
                        db.SaveChanges();

                        Answer a = new Answer();
                        a.Name = qa.nameAnswer;
                        a.id_tree = tree.id;
                        a.isValid = true;
                        a.id_question = q.id;
                        a.id_answer = qa.id_answer;
                        db.Answers.Add(a);
                        db.SaveChanges();
                    }
                    else
                    {
                        Tree t = db.Trees.Where(x => x.id_user == user.id_user).FirstOrDefault();
                        if (t == null)
                        {
                            t = new Tree();
                            t.id_user = user.id_user;
                            db.Trees.Add(t);
                            db.SaveChanges();
                        }
                            Question q = new Question();
                            q.isValid = true;
                            q.Name = qa.nameQuestion;
                            q.id_tree = t.id;
                            q.id_ques_node = qa.id_question;
                            db.Questions.Add(q);
                            db.SaveChanges();

                            Answer a = new Answer();
                            a.Name = qa.nameAnswer;
                            a.id_tree = t.id;
                            a.isValid = true;
                            a.id_question = q.id;
                            a.id_answer = qa.id_answer;
                            db.Answers.Add(a);
                            db.SaveChanges();
                    
                    }
                }
                Clients.Client(connId).addNewMessageToPage(answer, question, ans);

            }
            else
            {
                if (id < 21)
                {
                    string question = n.getQuestion();
                    question += "<input type='text' name='name1' id='name1'>";
                    question += "<input type='button' name='but' id='but' onclick='ispis()' value='Ok'>";
                    Clients.Client(connId).addNewMessageToPage(answer, question, "");
                }
                else
                {
                    User user1 = db.Users.FirstOrDefault(x => x.Username == System.Web.HttpContext.Current.User.Identity.Name);
                    if (user1 != null)
                    {
                        pr.id_user = user1.id_user;
                    }
                    db.PurchaseRequests.Add(pr);
                    db.SaveChanges();
                    string question = n.getQuestion();
                    //question += "<input type='text' name='name1' id='name1'>";
                    //question += "<input type='button' name='but' id='but' onclick='ispis()' value='Ok'>";
                    Clients.Client(connId).addNewMessageToPage(answer, question, "");
                }
            }


        }
    }
}