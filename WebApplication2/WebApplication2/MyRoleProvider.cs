﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Security;

namespace WebApplication2
{
    public class MyRoleProvider : RoleProvider
    {
        private int cacheTimeoutInMinute = 20;
        public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string username)
        {
            if(!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return null;
            }

            var cacheKey = string.Format("{0}_role",username);
            if(HttpRuntime.Cache[cacheKey]!=null)
            {
                return (string[])HttpRuntime.Cache[cacheKey];
            }
            string[] roles = new string[] { };
            using (ChatbotDBEntities4 db = new ChatbotDBEntities4())
            {
                 roles = (from a in db.Roles
                              join b in db.User_roles on a.id_role equals b.id_role
                              join c in db.Users on b.id_user equals c.id_user
                              where c.Username.Equals(username)
                              select a.Name).ToArray<string>();
                  if(roles.Count()>0)
                  {
                    HttpRuntime.Cache.Insert(cacheKey, roles, null, DateTime.Now.AddMinutes(cacheTimeoutInMinute),Cache.NoSlidingExpiration);
                   }

                
            }
            return roles;
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            var userRoles = GetRolesForUser(username);
            return userRoles.Contains(roleName);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}