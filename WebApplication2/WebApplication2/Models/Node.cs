﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApplication2.Service;
using System.Data;

namespace WebApplication2.Models
{
    public class Node
    {

        
        int idQuestion;
        string question;
        List<IQueryable> answers;
        int selectedAnswer; //id 
        public string nameOfTable;
        Dictionary<int, IQueryable> pitanjeUpit = new Dictionary<int, IQueryable>();
        public Node parent { get; set; }
        private List<Node> children;
        public Node Left;
        public Node Right;
        ChatbotDBEntities4 db = new ChatbotDBEntities4();
        DBService dbs = new DBService();
        int flag=-1; //1-levo, 0-desno

        //AnswerModel a = new AnswerModel();

        /*List<ListCategories_Result> cat;
        List<ClassOfVehicle_Result> clsCat;
        List<ChooseCharacteristic_Result> cchar;
        List<ChooseColor_Result> ccolor;
        List<ChooseFuel_Result> cfuel;
        List<ChooseManufacturer_Result> cmanufacturer;
        List<ChooseModel_Result> cmodel;
        List<ChooseMotorTypes_Result> cmotor;
        List<ChoosePackage_Result> cpackage;
        List<ChoosePrice_Result> cprice;
        List<Num_Seats_Result> nbseats;
        List<YearProduction_Result> yearprod;*/


        


        public Node(int id, string question, int flag, Node parent, string name)
        {
        
         
            this.idQuestion = id;
            this.question = question;
            this.parent = parent;
            this.Left = null;
            this.Right = null;
            this.flag = flag;
            this.nameOfTable = name;
            //pitanjeUpit.Add(0, yesNo.AsQueryable());
            //pitanjeUpit.Add(1, yesNo.AsQueryable());
        }


        public Node()
        {

        }

        public Node getLeft()
        {
            return Left;
        }

        public Node getRight()
        {
            return Right;
        }

        public Node getParent()
        {
            return this.parent;
        }

        public void setParent(Node n)
        {
            this.parent = n;
        }

        public string getNameTable()
        {
            return this.nameOfTable;
        }

        public void setNameTable(string name)
        {
            this.nameOfTable = name;
        }
        public string getQuestion()
        {
            return this.question;
        }

        public List<Object> getAnswers(AnswerModel a)
        {
            using (db)
            {


                //AnswerModel a = new AnswerModel();
                


                //===================================================================================================//
                List<string> yesNo = new List<string>();
                List<string> start = new List<string>();
                start.Add("Start chat");
                yesNo.Add("Yes");
                yesNo.Add("No");
                pitanjeUpit = new Dictionary<int, IQueryable>();
                pitanjeUpit.Add(1, yesNo.AsQueryable());
                pitanjeUpit.Add(2, a.List_category().AsQueryable()); //kategorije
                pitanjeUpit.Add(3, a.List_class().AsQueryable()); //podkategorije
                
                pitanjeUpit.Add(4, a.List_manufacturer().AsQueryable()); //proizvodjac
                pitanjeUpit.Add(5, a.List_model().AsQueryable()); //model
                pitanjeUpit.Add(6, a.List_characteristics().AsQueryable()); //karakteristike
                pitanjeUpit.Add(7, a.List_package().AsQueryable()); //paket*/
                pitanjeUpit.Add(8, yesNo.AsQueryable());                                                                                                                                                        
                //No grana
                pitanjeUpit.Add(9, a.List_category().AsQueryable()); // kategorija
                pitanjeUpit.Add(10, a.List_seats().AsQueryable()); //broj sedista
                pitanjeUpit.Add(11, a.List_mileage().AsQueryable()); //km  koliko prelazi vozilo
                pitanjeUpit.Add(12, a.List_colors().ToList().AsQueryable()); //boja
                pitanjeUpit.Add(13, a.List_fuels().AsQueryable()); //koje gorivo koristi
                pitanjeUpit.Add(14, a.List_motors().ToList().AsQueryable()); //tip motora
                pitanjeUpit.Add(15, a.List_years().AsQueryable()); //godina proizvodje
                pitanjeUpit.Add(16, a.List_price().AsQueryable()); //cena
      
                IQueryable t;
                System.Diagnostics.Debug.WriteLine(selectedAnswer);
                foreach (KeyValuePair<int, IQueryable> tabela in pitanjeUpit)
                {
                    System.Diagnostics.Debug.WriteLine(tabela.Key + " " + tabela.Value);

                    if (tabela.Key == idQuestion)
                    {
                        IQueryable ans;
                        ans = tabela.Value;
                        List<Object> lista = ans.Cast<dynamic>().ToList();

                        string s = ans.ToString();
                        return lista;
                    }
                }
                return null;
            }

        }

        public Node GetChild(int id)
        {
            return children[id];
        }


        public int getIdQuestion()
        {
            return this.idQuestion;
        }

        public void setIdQuestion(int idQ)
        {
            this.idQuestion = idQ;
        }

        public int getSelectedAnswer()
        {
            return this.selectedAnswer;
        }

        public void setSelectedAnswer(int id)
        {
            this.selectedAnswer = id;
        }

        public int getFlag()
        {
            return this.flag;
        }

        public void setFlag(int flag)
        {
            this.flag = flag;
        }

   /*     public string getQuestion()
        {
            return question;
        }*/

        public void setQuestion(string question)
        {
            this.question = question;
        }
        public void Insert(Node n, int flag)
        {
            
            if (flag==0)
            {
                if (this.Right == null)
                {
                    this.Right = n;
                }              
            }
            if (flag==1)
            {
                if (this.Left == null)
                {
                    this.Left = n;
                }              
            }
        }




    }
}