﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace WebApplication2.Models
{
    public class Tree
    {

        private Node root;
        private Node left;
        private Node right;
        private int depth;

        public Node Root { get { return root; } set { root = value; } }
        public Node Left { get { return left; } set { root = value; } }
        public Node Right { get { return right; } set { root = value; } }
        //int depth { get; set; }
        Node n1 = null;

        //int depth { get; set; }
        public Tree()
        {

        }

        public Tree(Node n)
        {
            Root = n;
        }

        public void setRoot(Node n)
        {
            Root = n;
        }

        //public Node getNode()
        //{
        //    return root;
        //}

        //public void setNode(Node node)
        //{
        //    this.root = node;
        //}
        public void insert(int id, string question, int flag)
        {
            root = insertData(root, id, question, flag);
        }

        Node insertData(Node root, int id, string question,int flag)
        {

            /* If the tree is empty, return a new node */
            if (root == null)
            {
         //       root = new Node(id,question,flag);
                return root;
            }

            /* Otherwise, recur down the tree */
            if (flag==1)
                root.Left = insertData(root.Left, id,question,flag);
            else if (flag==0)
                root.Right = insertData(root.Right, id, question,flag);

            /* return the (unchanged) node pointer */
            return root;
        }


        public Node search2(Node root, int s)
        {
            if (root == null)
            {
                return root;
            }

            if (root.getIdQuestion() == s)
            {
                return root;
            }
            else if (root.getIdQuestion() != s)
            {
                 n1= search2(root.Right, s);
            }
            else if (root.getIdQuestion() != s)
            {
                 n1= search2(root.Left, s);
            }

            return n1;
        }






        public void Print(Node p, int padding)
        {
            if (p != null)
            {
                if (p.Right != null)
                {
                    Print(p.Right, padding + 4);
                }
                if (padding > 0)
                {
                    System.Diagnostics.Debug.WriteLine(" ".PadLeft(padding));
                }
                if (p.Right != null)
                {
                    System.Diagnostics.Debug.WriteLine("/\n");
                    System.Diagnostics.Debug.WriteLine(" ".PadLeft(padding));
                }
                Console.Write(p.getFlag().ToString() + "\n ");
                if (p.Left != null)
                {
                    System.Diagnostics.Debug.WriteLine(" ".PadLeft(padding) + "\\\n");
                    Print(p.Left, padding + 4);
                }
            }
        }




    }
}