﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class AnswerModel
    {
        public int id_category = 1;
        public int id_class = 1;
        public int id_vehicle = 1;
        public int id_characteristics = 1;
        public int id_manufacturer = 1;
        public int id_model = 1;
        public int id_package = 1;
        
        
        public string color = "Brown";
        public string engine = "twe23";
        public int year_of_production = 2005;
        public int num_of_seats = 5;
        public double mileage = 15020.45;
        public string fuel = "diesel";
        public double price = 15020.45;

        ChatbotDBEntities4 db = new ChatbotDBEntities4();

        /*List<ListCategories_Result> cat;
        List<ClassOfVehicle_Result> clsCat;
        List<ChooseCharacteristic_Result> cchar;
        List<ChooseColor_Result> ccolor;
        List<ChooseFuel_Result> cfuel;
        List<ChooseManufacturer_Result> cmanufacturer;
        List<ChooseModel_Result> cmodel;
        List<ChooseMotorTypes_Result> cmotor;
        List<ChoosePackage_Result> cpackage;
        List<ChoosePrice_Result> cprice;
        List<Num_Seats_Result> nbseats;
        List<YearProduction_Result> yearprod;
        List<Mile_Result> mil;*/

        public AnswerModel()
        {
            /*id_category = 1;
            id_characteristics = 1;
            id_class = 1;
            id_manufacturer = 1;
            id_model = 1;
            id_package = 1;
            id_vehicle = 1;
            color = "Brown";
            engine = "twe23";
            year_of_production = 2005;
            num_of_seats = 5;
            mileage = 15020.45;
            fuel = "diesel";
            price = 0;*/
        }

        public void SetIdCategory(int id)
        {
            this.id_category = id;
        }
        public void SetIdClass(int id)
        {
            this.id_class = id;
        }
        public void SetIdManufacturer(int id)
        {
            this.id_manufacturer = id;
        }
        public void SetIdCharacteristics(int id)
        {
            this.id_characteristics = id;
        }
        public void SetIdModel(int id)
        {
            this.id_model = id;
        }
        public void SetIdPackage(int id)
        {
            this.id_package = id;
        }
        public void SetColor(string col)
        {
            this.color = col;
        }
        public void SetEngine(string eng)
        {
            this.engine = eng;
        }
        public void SetFuel(string fuel)
        {
            this.fuel = fuel;
        }
        public void SetNumOfSeats(int nod)
        {
            this.num_of_seats = nod;
        }
        public void SetMileage(double mile)
        {
            this.mileage = mile;
        }
        public void SetYearOfProduction(int yop)
        {
            this.year_of_production = yop;
        }
        public void SetIdVehicle(int id)
        {
            this.id_vehicle = id;
        }
        public void SetPrice(double pr)
        {
            this.price = pr;
        }

        public List<ListCategories_Result> List_category()
        {
            return db.ListCategories().ToList();
        }

        public List<ClassOfVehicle_Result> List_class()
        {
            return db.ClassOfVehicle(id_category).ToList();
        }

        public List<ChooseManufacturer_Result> List_manufacturer()
        {
            return db.ChooseManufacturer(id_category, id_class).ToList();
        }

        public List<ChooseModel_Result> List_model()
        {
            return db.ChooseModel(id_category, id_class, id_manufacturer).ToList();
        }

        public List<ChooseCharacteristic_Result> List_characteristics()
        {
            return db.ChooseCharacteristic(id_category, id_class, id_manufacturer, id_model).ToList();  
        }

        public List<ChoosePackage_Result> List_package()
        {
            return db.ChoosePackage(id_category, id_class, id_manufacturer, id_model, id_characteristics).ToList();
        }    

        public List<Num_Seats_Result> List_seats()
        {
            return db.Num_Seats(id_category).ToList();
        }

        public List<Mile_Result> List_mileage()
        {
            return db.Mile(num_of_seats, id_category).ToList();
        }

        public List<ChooseColor_Result> List_colors()
        {
            return db.ChooseColor(mileage, num_of_seats, id_category).ToList();
        }

        public List<ChooseFuel_Result> List_fuels()
        {
            return db.ChooseFuel(color, mileage, num_of_seats, id_category).ToList();
        }

        public List<ChooseMotorTypes_Result> List_motors()
        {
            return db.ChooseMotorTypes(color, mileage, num_of_seats, fuel, id_category).ToList();
        }

        public List<YearProduction_Result> List_years()
        {
            return db.YearProduction(engine, color, mileage, num_of_seats, fuel, id_category).ToList();            
        }

        public List<ChoosePrice_Result> List_price()
        {
            return db.ChoosePrice(engine, color, mileage, num_of_seats, fuel, id_category, year_of_production).ToList();
        }
    }
}