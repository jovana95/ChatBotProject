﻿public class QuestionAnswer
{
    public int idUser;
    public string nameQuestion;
    public string nameAnswer;
    public int idTree;
    public int id_question;
    public int id_answer;

    public QuestionAnswer(int id, string name, string nameA, int idq, int ida, int idTree)
    {
        this.idUser = id;
        this.nameQuestion = name;
        this.nameAnswer = nameA;
        this.idTree = id;
        this.id_question = idq;
        this.id_answer = ida;
    }
}