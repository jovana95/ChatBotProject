﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Register
    {
        [Required(ErrorMessage = "Firstname required.", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string Firstname { get; set; }
        [Required(ErrorMessage = "Lastname required.", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use letters only please")]
        public string Lastname { get; set; }
        [Required(ErrorMessage = "Email required.", AllowEmptyStrings = false)]
        [RegularExpression(@"^[^<>,?;:'()!~%-_#%/*""\s]+$", ErrorMessage = "Special characters except '.' and '@' not allowed")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Address required.", AllowEmptyStrings = false)]
        public string Address { get; set; }
        [Required(ErrorMessage = "Username required.", AllowEmptyStrings = false)]
        [System.Web.Mvc.Remote("IsUserExist", "Home", ErrorMessage = "Username already in use")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password required.", AllowEmptyStrings = false)]
        [RegularExpression(@"^[^<>.,?;:'()!~%-_@#%/*""\s]+$", ErrorMessage = "Special characters not allowed")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string Password { get; set; }
    }


}