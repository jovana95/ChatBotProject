//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication2
{
    using System;
    using System.Collections.Generic;
    
    public partial class User_roles
    {
        public int id { get; set; }
        public Nullable<int> id_role { get; set; }
        public Nullable<int> id_user { get; set; }
    
        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
