﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class MyAccountController : Controller
    {
        // GET: MyAccount
        public ActionResult Login()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login l, string ReturnUrl = "")
        {
            using (ChatbotDBEntities4 db = new ChatbotDBEntities4())
            {
                var user = db.Users.Where(a => a.Username.Equals(l.Username) && a.Password.Equals(l.Password)).FirstOrDefault();
                if (user != null)
                {
                    FormsAuthentication.SetAuthCookie(user.Username, l.RememberMe);
                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("MyProfile", "Home");
                    }
                }
            }
            ModelState.Remove("Password");
            return View();
        }




        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult EditProfile()
        {
            string username = User.Identity.Name;


            using (ChatbotDBEntities4 db = new ChatbotDBEntities4())
            {
                User user = db.Users.FirstOrDefault(u => u.Username.Equals(username));
                return View(user);
            }


        }

        [HttpPost]
        public ActionResult Edit(User u)
        {
            if (ModelState.IsValid)
            {
                ChatbotDBEntities4 db = new ChatbotDBEntities4();
                string username = User.Identity.Name;
                // Get the userprofile
                User user = db.Users.FirstOrDefault(a => u.Username.Equals(username));

                // Update fields
                user.FirstName = u.FirstName;
                user.LastName = u.LastName;
                user.Email = u.Email;
                user.Adress = u.Adress;
                user.Password = u.Password;
                user.Username = u.Username;

                db.Entry(user).State = EntityState.Modified;

                db.SaveChanges();

                return RedirectToAction("Index", "Home");
            }

            return View(u);
        }




    }
}