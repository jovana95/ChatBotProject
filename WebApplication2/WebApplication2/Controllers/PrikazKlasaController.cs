﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WebApplication2.Controllers
{
    public class PrikazKlasaController : ApiController
    {
        ChatbotDBEntities4 db = new ChatbotDBEntities4();

        [Route("api/list")]
        [AcceptVerbs("GET")]
        public IHttpActionResult Show(int id)
        {
            List<ClassOfCategory> lista = db.ClassOfCategories.Where(c => c.id_category == id).ToList();
            return Ok(lista);
        }

        [Route("api/listModels")]
        [AcceptVerbs("GET")]
        public IHttpActionResult ShowModels(int id)
        {
            List<Model_vehicle> listModels = db.Model_vehicle.Where(c => c.id_manufacturer == id).ToList();
            if (listModels.Count == 0)
            {
                return NotFound();
            }
            return Ok(listModels);
        }

        [Route("api/claassofcat/{id:int}/delete")]
        [AcceptVerbs("DELETE", "GET")]
        public IHttpActionResult DeleteClass(int id)
        {
            ClassOfCategory cat = db.ClassOfCategories.Remove(db.ClassOfCategories.Find(id));
            db.SaveChanges();
            return Ok();
        }

        [Route("api/modelvehicle/{id:int}/delete")]
        [AcceptVerbs("DELETE", "GET")]
        public IHttpActionResult DeleteModel(int id)
        {
            Model_vehicle mv = db.Model_vehicle.Remove(db.Model_vehicle.Find(id));
            db.SaveChanges();
            Redirect("http://localhost:63555/Home/AdminIndex");
            return Ok();
        }
    }
}