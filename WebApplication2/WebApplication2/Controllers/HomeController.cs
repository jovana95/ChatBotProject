﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        ChatbotDBEntities4 db = new ChatbotDBEntities4();
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult MyProfile()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }


        public JsonResult IsUserExist(string username)
        {
            return Json(!db.Users.Any(x => x.Username == username), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            var ListData = db.Categories.ToList();
            ViewBag.List = new SelectList(ListData, "Id", "Name", "0");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Register r, string ReturnUrl = "")
        {
            using (ChatbotDBEntities4 db = new ChatbotDBEntities4())
            {
                var user = db.Users.Where(a => a.Username.Equals(r.Username) || a.Password.Equals(r.Password)).FirstOrDefault();
                if (user == null)
                {
                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        db.Users.Add(new WebApplication2.User { Username = r.Username, Password = r.Password, Email = r.Email, Adress = r.Address, FirstName = r.Firstname, LastName = r.Lastname });
                        db.SaveChanges();
                        return RedirectToAction("MyProfile", "Home");
                    }
                }
                else
                {
                    Response.Write("Username or password already exists in database.");
                }
            }
            ModelState.Remove("Password");
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AdminIndex()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("LogOn", "Account");
            }
            using (db)
            {
                AdminViewModel myModel = new AdminViewModel();
                List<Characteristic> c = (from a in db.Characteristics select a).ToList();
                myModel.characteristic = c;
                List<ClassOfCategory> c1 = (from a in db.ClassOfCategories select a).ToList();
                myModel.classes = c1;
                List<Model_vehicle> mv = (from a in db.Model_vehicle select a).ToList();
                myModel.models = mv;
                List<Manufacturer> m = (from a in db.Manufacturers select a).ToList();
                myModel.manufacturer = m;
                List<Package> p = (from a in db.Packages select a).ToList();
                myModel.package = p;
                List<Vehicle> v = (from a in db.Vehicles select a).ToList();
                myModel.vehicle = v;
                List<Category> c2 = (from a in db.Categories select a).ToList();
                myModel.category = c2;
                return View(myModel);
            }
        }

        [Authorize]
        public ActionResult EditCategory(int id)
        {


            Category c = db.Categories.FirstOrDefault(x => x.id == id);
            return View(c);

        }

        [HttpPost]
        public ActionResult EditCategory(Category c)
        {

            Category c1 = db.Categories.FirstOrDefault(x => x.id == c.id);
            List<Category> lista = db.Categories.Where(x => x.Name == c.Name).ToList();
            if (lista.Count == 0)
            {
                c1.Name = c.Name;
                db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Category updated!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Category already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
        }

        public ActionResult EditModel(int id)
        {
            var sqlLista = (from p in db.Model_vehicle
                            join c1 in db.Manufacturers on p.id_manufacturer equals c1.id
                            select c1).Distinct().ToList();
            SelectList s = new SelectList(sqlLista, "id", "Name");
            Model_vehicle c = db.Model_vehicle.FirstOrDefault(x => x.id == id);
            ViewBag.FilterManu = s;


            return View(c);
        }

        [HttpPost]
        public ActionResult EditModel(Model_vehicle c)
        {

            Model_vehicle c1 = db.Model_vehicle.FirstOrDefault(x => x.id == c.id);
            List<Model_vehicle> lista = db.Model_vehicle.Where(x => x.Name == c.Name && x.year_of_production == c.year_of_production).ToList();
            if (lista.Count == 0)
            {
                c1.Name = c.Name;
                c1.year_of_production = c.year_of_production;
                c1.id_manufacturer = c.id_manufacturer;
                db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Model updated!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Model already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }

        }

        public ActionResult EditClasses(int id)
        {


            ClassOfCategory c = db.ClassOfCategories.FirstOrDefault(x => x.id == id);
            return View(c);
        }

        [HttpPost]
        public ActionResult EditClasses(ClassOfCategory c)
        {
            ClassOfCategory c1 = db.ClassOfCategories.FirstOrDefault(x => x.id == c.id);
            List<ClassOfCategory> lista = db.ClassOfCategories.Where(x => x.Name == c.Name).ToList();
            if (lista.Count == 0)
            {
                c1.Name = c.Name;
                db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Class updated!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Class already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }

        }

        public ActionResult EditManufacturer(int id)
        {

            Manufacturer c = db.Manufacturers.FirstOrDefault(x => x.id == id);
            return View(c);
        }

        [HttpPost]
        public ActionResult EditManufacturer(Manufacturer c)
        {
            Manufacturer c1 = db.Manufacturers.FirstOrDefault(x => x.id == c.id);
            List<Manufacturer> lista = db.Manufacturers.Where(x => x.Name == c.Name).ToList();
            if (lista.Count == 0)
            {
                c1.Name = c.Name;
                db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Manufacturer updated!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Manufacturer already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
        }


        public ActionResult EditPackage(int id)
        {
            var sqlLista = (from p in db.Packages
                            join c1 in db.Categories on p.id_category equals c1.id
                            select c1).ToList();
            SelectList s = new SelectList(sqlLista, "id", "Name");
            Package c = db.Packages.FirstOrDefault(x => x.id == id);
            ViewBag.Filter = s;
            return View(c);
        }

        [HttpPost]
        public ActionResult EditPackage(Package c)
        {

            Package c1 = db.Packages.FirstOrDefault(x => x.id == c.id);
            List<Package> lista = db.Packages.Where(a => a.id == c.id).ToList();
            if (lista.Count == 1)
            {
                c1.Price = c.Price;
                c1.seats_type = c.seats_type;
                c1.wheels = c.wheels;
                c1.air_conditioning = c.air_conditioning;
                c1.airbags = c.airbags;
                c1.fuel = c.fuel;
                c1.engine = c.engine;
                c1.id = c.id;
                c1.id_category = c.id_category;
                db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Package edited!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Same package already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");

            }
        }



        public ActionResult EditCharacteristic(int id)
        {

            var sqlCat = (from p in db.Packages
                          join c1 in db.Categories on p.id_category equals c1.id
                          select c1).Distinct().ToList();

            var sqlPac = (from ch in db.Characteristics
                          join pa in db.Packages on ch.id_package equals pa.id
                          select pa).Distinct().ToList();

            var sqlMod = (from ch1 in db.Characteristics
                          join mo in db.Model_vehicle on ch1.id_model equals mo.id
                          select mo).Distinct().ToList();

            SelectList s = new SelectList(sqlCat, "id", "Name");
            SelectList pa1 = new SelectList(sqlPac, "id", "id");
            SelectList m = new SelectList(sqlMod, "id", "Name");

            ViewBag.FilterCat = s;
            ViewBag.FilterPac = pa1;
            ViewBag.FilterMod = m;

            Characteristic c = db.Characteristics.FirstOrDefault(x => x.id == id);
            return View(c);
        }

        [HttpPost]
        public ActionResult EditCharacteristic(Characteristic c)
        {

            Characteristic c1 = db.Characteristics.FirstOrDefault(x => x.id == c.id);
            List<Characteristic> lista = db.Characteristics.Where(a => a.id == c.id).ToList();
            if (lista.Count == 1)
            {
                c1.doors = c.doors;
                c1.quantity = c.quantity;
                c1.color = c.color;
                c1.mileage = c.mileage;
                c1.number_of_seats = c.number_of_seats;
                c1.id_package = c.id_package;
                c1.id_category = c.id_category;
                c1.id_model = c.id_model;
                db.Entry(c1).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Edited!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Characteristic already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
        }


        public ActionResult DeleteCategory(int id)
        {

            Category c = db.Categories.FirstOrDefault(x => x.id == id);
            List<ClassOfCategory> lista = db.ClassOfCategories.Where(x => x.id_category == id).ToList();
            foreach (ClassOfCategory cc in lista)
            {
                db.ClassOfCategories.Remove(cc);
            }
            db.Categories.Remove(c);
            db.SaveChanges();
            return Content("<script language='javascript' type='text/javascript'>alert('Category deleted!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");

        }

        public ActionResult DeleteClass(int id)
        {

            ClassOfCategory c = db.ClassOfCategories.FirstOrDefault(x => x.id == id);
            db.ClassOfCategories.Remove(c);
            db.SaveChanges();
            return Content("<script language='javascript' type='text/javascript'>alert('Class deleted!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");

        }

        public ActionResult DeleteModels(int id)
        {


            Model_vehicle c = db.Model_vehicle.FirstOrDefault(x => x.id == id);
            db.Model_vehicle.Remove(c);
            db.SaveChanges();
            return Content("<script language='javascript' type='text/javascript'>alert('Model deleted!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
        }

        public ActionResult DeleteManufacturer(int id)
        {

            Manufacturer c = db.Manufacturers.FirstOrDefault(x => x.id == id);
            List<Model_vehicle> lista = db.Model_vehicle.Where(x => x.id == id).ToList();
            foreach (Model_vehicle mv in lista)
            {
                db.Model_vehicle.Remove(mv);
            }
            db.Manufacturers.Remove(c);
            db.SaveChanges();
            return Content("<script language='javascript' type='text/javascript'>alert('Manufacture deleted!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
        }

        public ActionResult DeletePackage(int id)
        {

            Package c = db.Packages.FirstOrDefault(x => x.id == id);
            db.Packages.Remove(c);
            db.SaveChanges();
            return Content("<script language='javascript' type='text/javascript'>alert('Package deleted!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");

        }

        public ActionResult DeleteCharacteristic(int id)
        {


            Characteristic c = db.Characteristics.FirstOrDefault(x => x.id == id);
            db.Characteristics.Remove(c);
            db.SaveChanges();
            return Content("<script language='javascript' type='text/javascript'>alert('Characteristic deleted!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");

        }

        public ActionResult AddCategory()
        {

            Category c = new Category();
            return View(c);
        }

        public ActionResult AddCategory1(Category c)
        {

            List<Category> pom = db.Categories.Where(a => a.Name == c.Name).ToList();
            if (pom.Count == 0)
            {
                db.Categories.Add(new Category { Name = c.Name });
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Category Added!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");

            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex'</script>");

            }
        }

        public ActionResult AddClass(int id)
        {


            ClassOfCategory c = new ClassOfCategory();
            c.id_category = id;
            return View(c);
        }

        //public JsonResult IsClassExist(int id)
        //{
        //    
        //    return Json(!db.ClassOfCategories.Any(x => x.id_class == id), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult AddClass1(ClassOfCategory c)
        {

            List<ClassOfCategory> lista = db.ClassOfCategories.Where(a => a.id == c.id).ToList();
            if (lista.Count == 0)
            {
                db.ClassOfCategories.Add(c);
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Class Added!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Class already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex'</script>");
            }

        }

        public ActionResult AddModel(int id)
        {


            Model_vehicle c = new Model_vehicle();
            c.id_manufacturer = id;
            return View(c);
        }

        public ActionResult AddModel1(Model_vehicle c)
        {

            List<Model_vehicle> lista = db.Model_vehicle.Where(a => a.year_of_production == c.year_of_production && a.Name == c.Name).ToList();
            if (lista.Count == 0)
            {
                db.Model_vehicle.Add(c);
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Model added!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Model already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex'</script>");
            }

        }
        public ActionResult AddManufacturer()
        {


            Manufacturer c = new Manufacturer();
            return View(c);
        }

        //public JsonResult IsManufacturerExist(string nameManufacturer)
        //{
        // 
        //    return Json(!db.Manufacturers.Any(x => x.Name_manufacturer == nameManufacturer), JsonRequestBehavior.AllowGet);
        //}

        public ActionResult AddManufacturer1(Manufacturer c)
        {

            List<Manufacturer> lista = db.Manufacturers.Where(a => a.Name == c.Name).ToList();
            if (lista.Count == 0)
            {
                db.Manufacturers.Add(c);
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Manufacture Added!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Manufacture already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex'</script>");
            }

        }

        public ActionResult AddPackage()
        {
            var sqlLista = (from p in db.Packages
                            join c in db.Categories on p.id_category equals c.id
                            select c).ToList();
            SelectList s = new SelectList(sqlLista, "id", "Name");
            Package pa = new Package();
            ViewBag.Filter = s;
            return View(pa);
        }


        public ActionResult AddPackage1(Package c)
        {
            ViewBag.GenderId = new SelectList(db.Categories, "GenderId", "Description");

            List<Package> lista = db.Packages.Where(a => a.Price == c.Price && a.fuel == c.fuel && a.airbags == c.airbags && a.seats_type == c.seats_type &&
                                                         a.air_conditioning == c.air_conditioning && a.engine == c.engine && a.wheels == c.wheels && a.id_category == c.id_category).ToList();
            if (lista.Count == 0)
            {
                db.Packages.Add(c);
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Package added!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Package with same data already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
        }

        public ActionResult AddCharacteristic()
        {
            var sqlCat = (from p in db.Packages
                          join c in db.Categories on p.id_category equals c.id
                          select c).Distinct().ToList();

            var sqlPac = (from ch in db.Characteristics
                          join pa in db.Packages on ch.id_package equals pa.id
                          select pa).Distinct().ToList();

            var sqlMod = (from ch1 in db.Characteristics
                          join mo in db.Model_vehicle on ch1.id_model equals mo.id
                          select mo).Distinct().ToList();

            SelectList s = new SelectList(sqlCat, "id", "Name");
            SelectList pa1 = new SelectList(sqlPac, "id", "id");
            SelectList m = new SelectList(sqlMod, "id", "Name");

            ViewBag.FilterCat = s;
            ViewBag.FilterPac = pa1;
            ViewBag.FilterMod = m;
            Characteristic cate = new Characteristic();
            return View(cate);
        }

        public ActionResult AddCharacteristic1(Characteristic c)
        {

            List<Characteristic> lista = db.Characteristics.Where(a => a.mileage == c.mileage && a.id_package == c.id_package && a.number_of_seats == c.number_of_seats &&
                                                                       a.doors == c.doors && a.quantity == c.quantity && a.color == c.color && a.id_category == c.id_category && a.id_model == c.id_model).ToList();
            if (lista.Count == 0)
            {
                db.Characteristics.Add(c);
                db.SaveChanges();
                return Content("<script language='javascript' type='text/javascript'>alert('Characteristic added!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
            else
            {
                return Content("<script language='javascript' type='text/javascript'>alert('Characteristic already exist!');window.location.href = 'http://localhost:63555/Home/AdminIndex' ;</script>");
            }
        }

        public ActionResult Chat()
        {
            return View();
        }

    }
}